# Data Science & Machine Learning at AIM

The idea for this little repository is to have a first look at the basic Data Science & Machine Learning coding which is done at AIM. 

## Data Science

An examplary dataset of AIM can be found in the folder [data](./data). For the Data Science part You may take a look at first only at:

- The observations meta data ([y_train_1_2](./data/y_train_1_2.pickle), [y_train_APR](./data/y_train_APR.json), [y_train_II](./data/y_train_II.sqlite))


### Preprocessing

After loading the meta data files You should check for data corruption and afterwards concat the cleaned data. Then You should set up the possibility to filter the data by its 'date' and 'sum_raw_reads' columns independently. For this You can use the given scripts You may find in [data_filtering](./src/data_filtering.py).

### Visualization

Afterwards You should be plotting the data by using its geo data stored as 'lat' and 'lon' columns. For this You may use the given script which can be found in [data_visualization](./src/data_visualization.py).

### Further Coding

If You like You may also add a new preprocessing step that allows to filter the data by its 'habitat_type'. However, therefore it is necessary to clean the data firstly a bit more to get useful results.

## Machine Learning

An examplary dataset of AIM can be found in the folder [data](./data). It is sepparated into train and test data which both are made up by a structure of three subdata types that are interconnected:

1. The plain OTU numbers ([X_train](./data/X_train.pickle), [X_test](./data/X_test.pickle))
2. The observations meta data respectivley ([y_train_1_2](./data/y_train_1_2.pickle), [y_train_APR](./data/y_train_APR.json), [y_train_II](./data/y_train_II.sqlite))
3. The features names respectivley ([zeta_train](./data/zeta_train.pickle), [zeta_test](./data/zeta_test.pickle))

### Preprocessing

After loading the OTU numbers & Your already concated meta data & the features names You should merge the train and test data respectively. Fit any convenient machine learning model You prefer with the train data.

### Visualization

Use Your machine learning model to predict the test data. To visualize the outcome You may use the given script which can be found in [data_prediction](./src/data_prediction.py).

### Further Coding

If You like You may improve Your results especially if there can be an over- or underfitting bias detected.


## Integration tests

To be sure of a correct meta data handling there are some integration tests included in the folder [test](./test). One concerns about the mean values of the sum_raw_reads data and the other one takes a look at the geodata.

### Preprocessing

After loading the concated meta data You should get the integration tests to run properly.

## Remarks

All the code given here is just a hint of the general structure. Feel perfectly free to rewrite the methods on Your own if You prefer to.