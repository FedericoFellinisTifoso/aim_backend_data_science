# %%
import pandas as pd
import numpy as np

import pytest

# %% Test mean values for the sum_raw_reads parts of the data
def test_mean_sum_raw_reads():
    """
    A test to check the mean values for the sum_raw_reads parts of the data

    expected
    ----------
    The expected mean values for the monthly sum_raw_reads parts of the data

    assert
    ----------
    Check for equal mean values for the monthly sum_raw_reads parts of the data
    """

    expected = {
        "April" : 24307,
        "May" : 22650,
        "July" : 26390,
        "August" : 18715,
    }

    actual = {
        month : "Get mean values for the sum_raw_reads parts of the data"
            for month in ["April", "May", "July", "August"]
    }

    assert expected == actual

# %% Test lat / lon values as the data should be coming from southern germany
def test_geodata_is_from_southern_germany():
    """
    A test to check the lat / lon values as the data should be coming from southern germany

    expected
    ----------
    The expected lat / lon values that confirm the data is coming from southern germany

    assert
    ----------
    The actual lat / lon values should range within the expected lat / lon boundaries
    """

    assert expected["lat_infimum"] <= actual["lat_minimum"]
    assert actual["lat_maximum"] <= expected["lat_supremum"]
    
    assert expected["lon_infimum"] <= actual["lon_minimum"]
    assert actual["lon_maximum"] <= expected["lon_supremum"]
    