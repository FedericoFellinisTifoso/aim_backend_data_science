# %%
import pandas as pd
import numpy as np

import plotly.graph_objects as go

# %% Get monthly parts of the data
def get_month(y : pd.core.frame.DataFrame, month : str):
    """
    A method to filter the data by a given month

    Parameter
    ----------
    y : pandas.core.frame.DataFrame
        a pandas dataframe containing the month data as 'date' column
    month : str
        the name of month for wich the data should be filtered

    Return
    ----------
    y : pandas.core.frame.DataFrame
        a pandas dataframe filtered by the given month
    """

    return y[y["date"].str.lower().str.contains(month.lower())] if "date" in y else pd.DataFrame()

# %% Get min sum_raw_reads parts of the data
def get_min_sum_raw_reads(y : pd.core.frame.DataFrame, threshold : int): 
    """
    A method to filter the data by a given sum_raw_reads threshold

    Parameter
    ----------
    y : pandas.core.frame.DataFrame
        a pandas dataframe containing the sum_raw_reads data as 'sum_raw_reads' column
    threshold : int
        the threshold that states the minimum amount of sum_raw_reads for wich the data should be filtered

    Return
    ----------
    y : pandas.core.frame.DataFrame
        a pandas dataframe filtered by the given sum_raw_reads threshold
    """

    return y[y["sum_raw_reads"] >= threshold] if "sum_raw_reads" in y else pd.DataFrame()