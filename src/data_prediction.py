# %%
import pandas as pd
import numpy as np

from sklearn.metrics import confusion_matrix, classification_report

import plotly.graph_objects as go

# %% Get geoplot
def get_prediction_heatmap(y_test : pd.core.frame.DataFrame, y_predict : pd.core.frame.DataFrame):
    """
    A method to plot the prediction results against the actual test values

    Parameter
    ----------
    y_test : pandas.core.frame.DataFrame
        a pandas dataframe containing actual test values
    y_predict : pandas.core.frame.DataFrame
        a pandas dataframe containing the prediction results

    Return
    ----------
    cl_report : sklearn.metrics.classification_report
        a text report showing the main classification metrics
    fig : plotly.graph_objs._figure.Figure
        a tree-like data structure heatmap along y_test and y_predict rendered by the plotly.js JavaScript library
    """

    y_target = y_test.columns[0]
    
    ticks = [tick for tick in sorted(list(set(y_test[y_target]).union(set(y_predict[y_target]))))]
    
    cl_report = classification_report(y_test, y_predict, zero_division=0, target_names = ticks)
    
    print(cl_report)

    conf_matrix = confusion_matrix(y_test, y_predict)

    fig = go.Figure(
        go.Heatmap(
            z=conf_matrix,
            x=[f"{tick}_p" for tick in ticks],
            y=ticks,
            text=conf_matrix,
            texttemplate="%{text}",
            textfont={"size":20},
            colorscale="Magma",
            )
    )

    return fig, cl_report