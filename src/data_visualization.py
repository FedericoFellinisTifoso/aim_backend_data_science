# %%
import pandas as pd
import numpy as np

import plotly.graph_objects as go

# %% Get geoplot
def get_geoplot(y : pd.core.frame.DataFrame, title_string : str = "GeoData"):
    """
    A method to plot geo data mapped on a open-street-map layer

    Parameter
    ----------
    y : pandas.core.frame.DataFrame
        a pandas dataframe containing the geo data within 2 separate columns named 'lat' and 'lon' 
    title_string : str
        the name of plot to be stored in folder 'visualizations/

    Return
    ----------
    fig : plotly.graph_objs._figure.Figure
        a tree-like data structure along the columns named 'lat' and 'lon' rendered by the plotly.js JavaScript library
    """

    if "lat" in y and "lon" in y:

        fig = go.Figure(
            go.Scattermapbox(
                lat=y["lat"],
                lon=y["lon"],
                mode='markers',
                customdata = y.values,
                hovertemplate=' '.join([col + ": %{customdata[" + str(i) + "]}<br>" for i, col in enumerate(y)]) +
                    "<extra></extra>",       
            ))

        fig.update_traces(marker=dict(size=8))
        fig.update_layout(mapbox_style="open-street-map", 
                            mapbox_zoom=4, 
                            mapbox_center=go.layout.mapbox.Center(
                                            lat=50,
                                            lon=11
                ))
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.95
        ))
        fig.update_layout(margin={"r":100,"t":0,"l":0,"b":0})

        return fig

    else:

        go.Figure()